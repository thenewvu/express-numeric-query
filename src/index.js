'use strict';

const _ = require('lodash');

const NUMBER_PATTERN = /^[-+]?\d+\.?\d*$/;

/**
 * Convert recursively number-liked values to numbers.
 * @param v
 * @returns {*}
 */
function convert(v) {
  if (_.isString(v) && NUMBER_PATTERN.test(v)) {
    return _.toNumber(v);
  }
  if (_.isObjectLike(v)) {
    v = _.isArray(v) ? _.map(v, convert) : _.mapValues(v, convert);
  }
  return v;
}

/**
 * Return an express.js middlewares that converts recursively
 * number-liked values to numbers.
 * @returns {Function}
 */
function expressNumericQuery() {
  return (req, res, next) => {
    req.query = convert(req.query);
    next();
  };
}

module.exports = expressNumericQuery;
