'use strict';

const path    = require('path');
const chai    = require('chai');
const fs      = require('fs');
const yaml    = require('js-yaml');
const decache = require('decache');
const expect  = chai.expect;

function runTests(tests) {
  Object.keys(tests).forEach((describeName) => {
    describe(describeName, function () {
      Object.keys(tests[describeName]).forEach((itName) => {
        it(itName, function (done) {
          const itData = tests[describeName][itName];
          decache('../src/index.js');
          const expressNumericQuery = require('../src/index.js');
          expressNumericQuery()(itData.req, null, (err) => {
            expect(err).to.be.undefined;
            expect(itData.req).to.deep.equal(itData.res);
            done();
          });
        });
      });
    });
  });
}

const testDir   = path.dirname(__filename);
const testFiles = fs.readdirSync(testDir);
testFiles.forEach(filename => {
  if (filename.endsWith('.yaml')) {
    const filepath  = path.join(testDir, filename);
    const testData  = fs.readFileSync(filepath);
    const testCases = yaml.load(testData);
    runTests(testCases);
  }
});

